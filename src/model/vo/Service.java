package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	private String trip_id;
	private String taxi_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;

	public int getDropoff_community_area() {
		return dropoff_community_area;
	}

	public void setDropoff_community_area(int dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}

	private int dropoff_community_area;
	public Service(String id, String taxid, int sec, double miles, double total, int dropoff_community_area) {
		// TODO Auto-generated constructor stub
		trip_id=id;
		taxi_id=taxid;
		trip_seconds=sec;
		trip_miles=miles;
		trip_total=total;
		this.dropoff_community_area = dropoff_community_area;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id "+trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id "+taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
