package model.logic;

import api.ITaxiTripsManager;
import com.google.gson.Gson;
import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.vo.Service;
import model.vo.Taxi;

import java.io.FileReader;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		
		try {
			Taxi[] arreglo = gson.fromJson(new FileReader(serviceFile), Taxi[].class);
			for(int i =0; i<arreglo.length;i++) {
				taxis.add(arreglo[i]);
			}
			Service[] arreglo2 = gson.fromJson(new FileReader(serviceFile), Service[].class);
			for(int i=0; i<arreglo2.length;i++) {
				services.add(arreglo2[i]);
			}
		}
		catch(Exception e) {
			
		}
		
		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		//System.out.println("Inside getTaxisOfCompany with " + company);
		LinkedList<Taxi> taxisComp = new DoubleLinkedList<>();
		Node<Taxi> actual= taxis.getCabeza();
		Node<Taxi> actualComp= taxisComp.getCabeza();
		String s = "company: "+ company;
		while(actual!=null) {
			if(actual.getElement().getCompany().equalsIgnoreCase(s)) {
				if(!taxisComp.isEmpty()) {
					boolean metalo=true;
					while(actualComp!=null){
						if(actualComp.getElement().getTaxiId().equals(actual.getElement().getTaxiId())) {
							metalo=false;
							break;
						}

						actualComp=actualComp.getNext();
					}
					actualComp=taxisComp.getCabeza();
					if(metalo) {
						taxisComp.add(actual.getElement());
					}
				}
				else {
					taxisComp.add(actual.getElement());
				}
			}
			actual=actual.getNext();
		}
		return taxisComp;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		LinkedList<Service> serviciosCom = new DoubleLinkedList<>();
		Node<Service> act=services.getCabeza();
		while(act!=null){
			int comAr=act.getElement().getDropoff_community_area();
			if(comAr==communityArea){
				serviciosCom.add(act.getElement());
			}
			act=act.getNext();
		}
		return serviciosCom;
	}
}
