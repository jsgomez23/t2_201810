package model.data_structures;


public class Node<E> {
	private Node<E> next;
	private Node<E> prev;
	private E element;
	public Node(Node<E> sig, Node<E> pre, E ele){
		next=sig;
		prev = pre;
		element = ele;
	}
	public Node<E> getNext() {
		return next;
	}
	public void setNext(Node<E> next) {
		this.next = next;
	}
	public Node<E> getPrev() {
		return prev;
	}
	public void setPrev(Node<E> prev) {
		this.prev = prev;
	}
	public E getElement() {
		return element;
	}
	public void setElement(E element) {
		this.element = element;
	}
	
}
