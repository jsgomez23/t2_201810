package model.data_structures;

public class DoubleLinkedList<T extends Comparable<T>> implements LinkedList<T> {

	private int size;

	public Node<T> getActual() {
		return actual;
	}

	public void setActual(Node<T> actual) {
		this.actual = actual;
	}

	public Node<T> getCabeza() {
		return cabeza;
	}

	public void setCabeza(Node<T> cabeza) {
		this.cabeza = cabeza;
	}

	public Node<T> getCola() {
		return cola;
	}

	public void setCola(Node<T> cola) {
		this.cola = cola;
	}

	private Node<T> actual;
	private Node<T> cabeza;
	private Node<T> cola;
	public DoubleLinkedList() {
		size=0;
		cola=null;
		cabeza=null;
		actual =null;// TODO Auto-generated constructor stub
	}
	@Override
	public void add(T element) {
		if(isEmpty()) {
			cabeza = new Node<T>(null, null, element);
			cola = cabeza;
			actual =cabeza;
			// TODO Auto-generated method stub
		}
		else {
			Node<T> nuevo = new Node<T>(null, cola, element);
			cola.setNext(nuevo);
			cola = nuevo;
		}
		size++;
	}

	@Override
	public void delete(T element) {
		// TODO Auto-generated method stub
		Node<T> aEliminar=cabeza.getNext();
		if(aEliminar.equals(element)){
			cabeza=cabeza.getNext();
			cabeza.setPrev(null);
		}
		else if(cola.getElement().equals(element)){
			cola=cola.getPrev();
			cola.setNext(null);
		}
		else{
			while(aEliminar!=null){
				if(aEliminar.getElement().equals(element)){
					Node<T> sig = aEliminar.getNext();
					Node<T> prev= aEliminar.getPrev();
					sig.setPrev(prev);
					prev.setNext(sig);
				}
			}
		}
	}

	@Override
	public boolean contains(T element) {

		if(size!=0){
			Node<T> act = cabeza;
			while(act!=null){
				if(act.getElement().equals(element)){
					return true;
				}
				act=act.getNext();
			}
		}
		return false;
	}

	@Override
	public T get(int pos) {
		// TODO Auto-generated method stub
		T element;
		Node<T> act = cabeza;
		if(pos>=size||pos<0){
			return null;
		}
		else{
			for(int i=0; i<pos;i++){
				act = act.getNext();
			}
			element= act.getElement();
			return element;
		}

	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public Node<T> next() {
		// TODO Auto-generated method stub
		actual=actual.getNext();
		return actual;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size==0);
	}
	@Override
	public Node<T> getCurrent() {
		// TODO Auto-generated method stub
		return actual;
	}
	public Node<T> prev(){
		actual=actual.getPrev();
		return actual;
	}
}
